import org.junit.gen5.api.Test;
import ua.od.atomspace.EmployeeInfoValidation;

import static org.junit.gen5.api.Assertions.*;

public class ValidationTest {
    EmployeeInfoValidation employeeInfoValidation = new EmployeeInfoValidation();
    @Test
    public void testPosition() throws Exception {
        boolean junior = employeeInfoValidation.positionValidation("junior");
        assertEquals(true, junior);
        boolean middle = employeeInfoValidation.positionValidation("middle");
        assertEquals(true, middle);
        boolean senior = employeeInfoValidation.positionValidation("senior");
        assertEquals(true, senior);
        boolean lead = employeeInfoValidation.positionValidation("lead");
        assertEquals(true, lead);
        boolean crashTest = employeeInfoValidation.positionValidation("teamLead");
        assertFalse(crashTest);
    }
    @Test
    public void testNumber() throws Exception {
        String[] numbers = {"+3809261234567",
                "09261234567",
                "79261234567",
                "+380 926 123 45 67",
                "0(926)123-45-67",
                "123-45-67",
                "9261234567",
                "79261234567",
                "(495)1234567",
                "(495) 123 45 67",
                "09261234567",
                "0-926-123-45-67",
                "0 927 1234 234",
                "0 927 12 12 888",
                "0 927 12 555 12",
                "0 927 123 8 123",};
        for(int i = 0; i < numbers.length;i++) {
            assertTrue(employeeInfoValidation.numberValidation(numbers[i]));
        }
        assertFalse(employeeInfoValidation.numberValidation("string"));
        assertFalse(employeeInfoValidation.numberValidation("333"));
        assertFalse(employeeInfoValidation.numberValidation("100000000000000000000000"));
        assertFalse(employeeInfoValidation.numberValidation("380.73.40.92261"));
    }
    @Test
    public void testName() {
        String[] names = {
                "Петров Петр Петрович",
                "Петров-Черный Петр Петрович",
                "И Иван Иванович",
                "Ли Лу Янг",
                "Dwain Simmons",
                "Dwain-Branden Simmons",
                "Салим-оглы Мамед",
                "Салим-кызы Лейла",
        };
        for(int i = 0; i < names.length;i++) {
            assertTrue(employeeInfoValidation.nameValidation(names[i]));
        }
        assertFalse(employeeInfoValidation.numberValidation("Name"));
        assertFalse(employeeInfoValidation.numberValidation("name name name"));
        assertFalse(employeeInfoValidation.numberValidation("name Name Name"));
        assertFalse(employeeInfoValidation.numberValidation("Name name Name"));
        assertFalse(employeeInfoValidation.numberValidation("380 Name"));
        assertFalse(employeeInfoValidation.numberValidation("N_ame |name"));
    }
    @Test
    public void testAgeInCompany() throws Exception {
        for(int i = 1;i<100;i++) {
            assertTrue(employeeInfoValidation.ageInCompanyValidation(i));
        }
        assertFalse(employeeInfoValidation.ageInCompanyValidation(101));
        assertFalse(employeeInfoValidation.ageInCompanyValidation(-2));
    }
}
