package ua.od.atomspace;

public class Employee {
    private String name;
    private String position;
    private String number;
    private int ageInCompany;
    protected int salary;
    public Employee(String name, String position, String number, int ageInCompany) {
        this.name = name;
        this.position = position;
        this.number = number;
        this.ageInCompany = ageInCompany;
        this.salary = setSalaryForPosition();
    }

    public String getName() {
        return name;
    }

    public String getPosition() {
        return position;
    }

    public String getNumber() {
        return number;
    }

    public int getAgeInCompany() {
        return ageInCompany;
    }

    public int getSalary() {
        return salary;
    }

    protected void setPosition(String position) {
        this.position = position;
    }

    private int setSalaryForPosition() {
        int positionSalary = 0;
        if(position.equals("junior")) {
            positionSalary = 200;
        }
        if(position.equals("middle")) {
            positionSalary = 300;
        }
        if(position.equals("senior")) {
            positionSalary = 400;
        }
        if(position.equals("lead")) {
            positionSalary = 500;
        }
        return positionSalary;
    }

    public String employeeInfo() {
        return name + "," + position + "," + number + "," + ageInCompany + "," + salary;
    }
}
