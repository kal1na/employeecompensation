package ua.od.atomspace;

import java.util.*;

public class EmployeeConsoleReader extends EmployeeReader {
    Scanner in = new Scanner(System.in);
    public EmployeeConsoleReader() {
        employeeList = new ArrayList<Employee>();
        employeeMap = new TreeMap<String, Employee>();
    }
    EmployeeInfoValidation employeeInfoValidation = new EmployeeInfoValidation();
    @Override
    public List<Employee> getEmployeeList() {
        return employeeList;
    }
    @Override
    public Map<String, Employee> getEmployeeMap() {
        return employeeMap;
    }

    private String enterPosition() {
        System.out.println("Enter position: ");
        String position = in.next();
        if(employeeInfoValidation.positionValidation(position)) {

        }
        else {
            System.out.println("No valid try again.");
            position = enterPosition();
        }
        return position;
    }

    private int enterAgeInCompany() {
        System.out.println("Enter age in company:");
        int ageInCompany = in.nextInt();
        if(employeeInfoValidation.ageInCompanyValidation(ageInCompany)) {

        }
        else {
            System.out.println("No valid try again.");
             ageInCompany = enterAgeInCompany();
        }
        return ageInCompany;
    }

    private String enterNumber() {
        System.out.println("Enter number:");
        String number = in.next();
        if(employeeInfoValidation.numberValidation(number)) {

        }
        else {
            System.out.println("No valid try again.");
            number = enterNumber();
        }
        return number;
    }

    private String enterName() {
        System.out.println("Enter name:");
        String name = in.nextLine();
        if(employeeInfoValidation.nameValidation(name)) {

        }
        else {
            System.out.println("No valid try again.");
            name = enterName();
        }
        return name;
    }
    public void addEmployee() {
        String name = enterName();
        String position = enterPosition();
        String number = enterNumber();
        int ageInCompany = enterAgeInCompany();
        employeeList.add(new Employee(name,position,number,ageInCompany));
        fillEmployeeNumberNameMap();
    }
    @Override
    public void fillEmployeeNumberNameMap() {
        employeeMap.clear();
        for(Employee empl : employeeList) {
            employeeMap.put(empl.getNumber(),empl);
        }
    }

}
