package ua.od.atomspace;

import java.io.File;
import java.util.Scanner;

public class EmployeCompensation {
    public static void main(String args[]) {
        File employeFile = new File("/home/kalin/IdeaProjects/EmployeeCompensation/src/main/java/ua/od/atomspace/employee.txt");
        EmployeeCompensationCalculateMenu menu = new EmployeeCompensationCalculateMenu(new EmployeeFileReader(employeFile));
        menu.startMenu();
    }
}
