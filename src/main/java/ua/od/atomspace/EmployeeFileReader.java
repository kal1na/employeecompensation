package ua.od.atomspace;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

public class EmployeeFileReader extends EmployeeReader {
    EmployeeInfoValidation employeeInfoValidation = new EmployeeInfoValidation();
    File file;
    public EmployeeFileReader(File file) {
        this.file = file;
        employeeList = new ArrayList<Employee>();
        employeeMap = new TreeMap<String, Employee>();
    }
    @Override
    public void fillEmployeeNumberNameMap() {
        employeeMap.clear();
        for(Employee empl : employeeList) {
            employeeMap.put(empl.getNumber(),empl);
        }
    }

    @Override
    public List<Employee> getEmployeeList() {
        return employeeList;
    }

    @Override
    public Map<String, Employee> getEmployeeMap() {
        return employeeMap;
    }

    @Override
    public void addEmployee() {
        try(BufferedReader br = new BufferedReader(new FileReader(file)))
        {
            //чтение построчно
            String employeeInformation;
            while((employeeInformation=br.readLine())!=null){
                String[] splitInfo = employeeInformation.split(",");
                if(employeeInfoValidation.allEmployeeValidation(splitInfo)) {
                    employeeList.add(new Employee(splitInfo[0],splitInfo[1],splitInfo[2],Integer.parseInt(splitInfo[3])));
                }
            }
        }
        catch(IOException ex){
            System.out.println(ex.getMessage());
        }
        fillEmployeeNumberNameMap();
    }
}
