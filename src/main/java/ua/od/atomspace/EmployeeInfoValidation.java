package ua.od.atomspace;

public class EmployeeInfoValidation {
    public boolean positionValidation(String employeePosition) {
        boolean validationResult = false;
        String[] positions = {"junior","middle","senior","lead"};
        for(int i = 0; i < positions.length;i++) {
            if(employeePosition.equals(positions[i])) {
                validationResult = true;
            }
        }
        return validationResult;
    }
    public boolean ageInCompanyValidation(int employeeAgeInCompany) {
        boolean validationResult = false;
        if(employeeAgeInCompany > 0 && employeeAgeInCompany < 100) {
            validationResult = true;
        }
        return validationResult;
    }
    public boolean numberValidation(String number) {
        boolean validationResult = number.matches("^((0|\\+380)[\\- ]?)?(\\(?\\d{3}\\)?[\\- ]?)?[\\d\\- ]{7,10}$");
        return validationResult;
    }
    public boolean nameValidation(String name) {
        boolean validationResult = name.matches("^([А-ЯA-Z]|[А-ЯA-Z][\\x27а-яa-z]{1,}|[А-ЯA-Z][\\x27а-яa-z]{1,}\\-([А-ЯA-Z][\\x27а-яa-z]{1,}|(оглы)|(кызы)))\\040[А-ЯA-Z][\\x27а-яa-z]{1,}(\\040[А-ЯA-Z][\\x27а-яa-z]{1,})?$");
        return validationResult;
    }
    public boolean allEmployeeValidation(String[] employeeInformation) {
        boolean validationResult = true;
        if(!nameValidation(employeeInformation[0])) {
            validationResult = nameValidation(employeeInformation[0]);
        }
        if(!positionValidation(employeeInformation[1])) {
            validationResult = positionValidation(employeeInformation[1]);
        }
        if(!numberValidation(employeeInformation[2])) {
            validationResult = numberValidation(employeeInformation[2]);
        }
        if(!ageInCompanyValidation(Integer.parseInt(employeeInformation[3]))) {
            validationResult = ageInCompanyValidation(Integer.parseInt(employeeInformation[3]));
        }
        return  validationResult;
    }
}
