package ua.od.atomspace;

import java.util.Scanner;

public class EmployeeCompensationCalculateMenu {
    EmployeeReader employeeReader;
    Scanner in = new Scanner(System.in);
    CalculateCompensation calculateCompensation;

    public EmployeeCompensationCalculateMenu(EmployeeReader employeeReader) {
        this.employeeReader = employeeReader;
        this.calculateCompensation = new CalculateCompensation(employeeReader.getEmployeeMap());
    }


    public void startMenu() {
        boolean end = true;
        while (end) {
            System.out.printf("1.Add employee\n2.Calculate compensation to number\n3.Exit\n");
            switch (in.nextInt()) {
                case 1:
                    employeeReader.addEmployee();
                    break;
                case 2:
                    System.out.println("/////Employee List/////");
                    checkEmployeeList();
                    String employeeNumber = in.next();
                    System.out.println("Compensation: " + calculateCompensation.calculateCompensationForOneEmployee(employeeNumber));
                    break;
                case 3:
                    end = false;
                    break;
            }
        }
    }

    private void checkEmployeeList() {
        for (Employee employee: employeeReader.getEmployeeList()) {
            System.out.println(employee.employeeInfo());
        }
    }

}
