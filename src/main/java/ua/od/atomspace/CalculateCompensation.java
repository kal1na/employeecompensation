package ua.od.atomspace;

import java.util.Map;

public class CalculateCompensation {
    Map<String,Employee> employeeMap;
    public CalculateCompensation(Map<String,Employee> employeeMap) {
        this.employeeMap = employeeMap;
    }
    public int calculateCompensationForOneEmployee(String number) {
        int compensation = 0;
        if(employeeMap.containsKey(number)) {
            Employee compensationInfo = employeeMap.get(number);
            for (int i = 0; i < compensationInfo.getAgeInCompany(); i++) {
                if (i > 7) {
                    compensationInfo.salary += 100;
                }
                if (compensationInfo.getPosition().equals("junior")) {
                    compensationInfo.salary += 200;
                }
                if (compensationInfo.getPosition().equals("middle")) {
                    compensationInfo.salary += 300;
                }
                if (compensationInfo.getPosition().equals("senior")) {
                    compensationInfo.salary += 400;
                }
                if (compensationInfo.getPosition().equals("lead")) {
                    compensationInfo.salary += 500;
                }
                compensation += compensationInfo.salary * 12;
            }
        }
        else {
            System.out.println("Check number and try again");
        }
        return compensation;
    }

}
