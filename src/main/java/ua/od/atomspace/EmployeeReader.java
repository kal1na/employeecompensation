package ua.od.atomspace;

import java.util.List;
import java.util.Map;

public abstract class EmployeeReader {
    public List<Employee> employeeList;
    public Map<String,Employee> employeeMap;
    public abstract void fillEmployeeNumberNameMap();
    public abstract List<Employee> getEmployeeList();
    public abstract Map<String, Employee> getEmployeeMap();
    public abstract void addEmployee();
}
